module.exports = {
   networks: {
  development: {
    host: "localhost",
    port: 8545,
    network_id: "*" // match any network
  },
  test: {
	host: "localhost",
    port: 9545,
    network_id: "*" // match any network
  }
}
};
