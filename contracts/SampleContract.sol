pragma solidity ^0.4.24;

import "./openzeppelin-solidity/Whitelist.sol";

contract SampleContract is Whitelist {

	uint256 private id;

	constructor() public {
		addAddressToWhitelist(msg.sender);
		id = 0;
	}
	
	function incrementId() public onlyWhitelisted(){
		id++;
	}
	
	function getId() public onlyWhitelisted() view returns(uint256){
		return id;
	}
}