const SampleContractMock = artifacts.require('SampleContract');

contract('SampleContract', function (accounts) {
  
	let mock;
  
	const [
		owner,
		anyone,
	] = accounts;

	before(async function () {
		mock = await SampleContractMock.new();
	});

	context('in normal conditions', () => {
		it('should return 0 after init ', async function () {
			const value = await mock.getId({ from: owner });
			assert.equal(value,0, 'THe value should be 0');
		});

		it('should return 1 after the first call', async function () {
			await mock.incrementId({ from: owner });
			const value = await mock.getId({ from: owner });
			console.log(value);
			assert.equal(value,1, 'Bad value');
		});
	});
});